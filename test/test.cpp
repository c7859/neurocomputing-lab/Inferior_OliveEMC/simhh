//
// Created by max on 01-04-21.
//
#define BOOST_TEST_MODULE SqrTests
#include <boost/test/unit_test.hpp>

#include "../src/includes/mgpuV2.h"

BOOST_AUTO_TEST_CASE(FailTest)
        {
            mgpuV2();
            BOOST_CHECK_EQUAL(2, 2);
        }
