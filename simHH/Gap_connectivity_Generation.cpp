//
// Created by max on 02-04-21.
// todo list:
//  - add more support jjeeej
//

#include "Gap_connectivity_Generation.h"
#include "GPU_kernels.h"
#include "CPU_kernels.h"
#include "miniLogger/miniLogger.h"
#include <cstdio>

#include <fstream>
#include <sstream> //file parsing
#include <string>


//explain
//todo connection in use moet veel beter ++ veel beter fixen van copy etc.

bool fn_random_binary_gap_generation(float Density, uint32_t randomseed, NetworkConstStruct &networkConst, process_info &Proccesinfo){
    INIT_LOG(LOG_DEFAULT);
    const uint32_t sample_scale   = (1 << 24); //how many bits wide should a random sample be?
    const uint32_t max_rand_value = 16777215;  //maximum rand value
    if(Density == 0) return false;
    float probability = (float)Density/(float)networkConst.nCells;

    uint32_t GPU_counter = 0;
    uint32_t GPU_Cell_counter = 0;

    auto *temp_cellID     = new std::vector<uint64_t>();
    temp_cellID->reserve((uint64_t)(Density*networkConst.nCells*1.5));

    for(size_t cellid_l = 0; cellid_l<networkConst.nCells_l; cellid_l++){
        uint64_t count_single = 0;
        XorShiftMul rng(((uint64_t) randomseed << 32) + cellid_l);
        //init a single cell generation:
        networkConst.CellPopulation[cellid_l].NeighCount = 0;
        //generation for this cell
        while(true) {
            for (size_t cellid_g = 0; cellid_g < networkConst.nCells; cellid_g++) {
                const float random_number = ((float) (rng.Get() % sample_scale) / (float) (max_rand_value));
                if (random_number <= probability) {
                    temp_cellID->push_back(cellid_g);
                    count_single++;
                    Proccesinfo.connections_inuse[cellid_g] = true; //watch race conditions here.
                    if(count_single >= Density){
                        goto ENDGENBIN;
                    }
                }
            }
        }

ENDGENBIN:
        networkConst.CellPopulation[cellid_l].NeighCount = count_single;
        Proccesinfo.total_count += count_single;
        if(Proccesinfo.GPU){
            if(cellid_l == networkConst.GPU_numbers[GPU_counter].nCells_gpu + GPU_Cell_counter){
                GPU_counter++;
                GPU_Cell_counter = cellid_l;
                fn_copy_neighID_array_gpu(networkConst.CellPopulation[0],temp_cellID->data(),temp_cellID->size() * sizeof(uint64_t),GPU_counter);
                temp_cellID->clear();
            }
        }
    }


    //copy to gpu and cpu
    if(Proccesinfo.GPU){
        fn_copy_neighID_array_gpu(networkConst.CellPopulation[GPU_Cell_counter],temp_cellID->data(),temp_cellID->size() * sizeof(uint64_t),GPU_counter);
    }else {
        networkConst.CellPopulation[0].NeighIds = new uint64_t[temp_cellID->size()];
        fn_copy_host_to_dev_cpu(networkConst.CellPopulation[0].NeighIds, temp_cellID->data(), temp_cellID->size() * sizeof(uint64_t));
    }


//todo pointer fixen
    if(Proccesinfo.GPU) {
        uint64_t  count = 0;
        uint32_t GPU_counter_n = 0;
        uint32_t GPU_Cell_counter_n =  0;
        for (size_t cellid_l = 0; cellid_l < networkConst.nCells_l; cellid_l++) {
            if (cellid_l == networkConst.GPU_numbers[GPU_counter].nCells_gpu + GPU_Cell_counter_n) {
                GPU_counter_n++;
                GPU_Cell_counter_n = cellid_l;
                count = 0;
            }
            networkConst.CellPopulation[cellid_l].NeighIds = &networkConst.CellPopulation[GPU_Cell_counter_n].NeighIds[count];
            count += networkConst.CellPopulation[cellid_l].NeighCount;
        }
    }
    else{
        uint64_t  count = 0;
        for (size_t cellid_l = 0; cellid_l < networkConst.nCells_l; cellid_l++) {
            networkConst.CellPopulation[cellid_l].NeighIds = &networkConst.CellPopulation[0].NeighIds[count];
            count += networkConst.CellPopulation[cellid_l].NeighCount;
        }
    }
    delete temp_cellID;
    return true;
}
bool fn_gaussian_gap_generation(uint32_t Density, float mean, float variance, uint32_t randomseed, NetworkConstStruct &networkConst, process_info &Proccesinfo){
    INIT_LOG(LOG_DEFAULT);
    const uint32_t sample_scale   = (1 << 24); //how many bits wide should a random sample be?
    const uint32_t max_rand_value = 16777215;  //maximum rand value
    if(Density == 0) return false;

    auto *probabilities = new float[networkConst.nCells];

//    if(Proccesinfo.GPU > 1){
//        //do tings differently !
//        exit(0);
//    }

    float xd = 1.0f / (variance * sqrtf(2 * M_PI));
    float yd = -(1.0f / (2.0f * powf(variance, 2.0f)));
    for (size_t i = 0; i < networkConst.nCells; i++) {
        probabilities[i] = xd * expf(powf((float)i - mean, 2) * yd);   //2 because distance can be either positive or negative.
    }
    auto *temp_cellID     = new std::vector<uint64_t>();
    temp_cellID->reserve((uint64_t)(Density*networkConst.nCells*1.5));


    uint32_t GPU_counter = 0;
    uint32_t GPU_Cell_counter = 0;
    for(size_t cellid_l = 0; cellid_l<networkConst.nCells_l; cellid_l++){

        uint64_t count_single = 0;
        XorShiftMul rng(((uint64_t) randomseed << 32) + cellid_l);
        //init a single cell generation:
        networkConst.CellPopulation[cellid_l].NeighCount = 0;

        //generation for this cell
        for(int retry = 1; retry <= 1; retry++) {
            for (size_t d = 1; d < networkConst.nCells; d++) {
                {
                    const uint32_t n_c = (cellid_l + networkConst.CellID_Offset) + d;
                    if(n_c < networkConst.nCells) {

                        const float probability = (float) probabilities[d] / xd;
                        const float random_number = ((float) (rng.Get() % sample_scale) / (float) (max_rand_value));
                        if (random_number <= probability) {
                            temp_cellID->push_back(n_c);
                            Proccesinfo.connections_inuse[n_c] = true; //watch race conditions here.
                            count_single++;
                        }
                        if (count_single >= Density){
                            goto ENDGEN;
                        }
                    }
                }
                {
                    const uint32_t n_c = (cellid_l + networkConst.CellID_Offset) - d;
                    if(cellid_l + networkConst.CellID_Offset >= d){

                        const float probability = (float) probabilities[d] / xd;

                        const float random_number = ((float) (rng.Get() % sample_scale) / (float) (max_rand_value));
                        if (random_number <= probability) {
                            temp_cellID->push_back(n_c);
                            Proccesinfo.connections_inuse[n_c] = true; //watch race conditions here.
                            count_single++;
                        }
                        if (count_single >= Density){
                            goto ENDGEN;
                        }
                    }
                }
            }
        }

    ENDGEN:
        networkConst.CellPopulation[cellid_l].NeighCount = count_single;
        Proccesinfo.total_count += count_single;
        log(LOG_DEBUG) << "{" << cellid_l << "," << networkConst.CellPopulation[cellid_l].NeighCount << "} -";
        if(Proccesinfo.GPU){
            if(cellid_l == networkConst.GPU_numbers[GPU_counter].nCells_gpu + GPU_Cell_counter){
                GPU_counter++;
                GPU_Cell_counter = cellid_l;
                fn_copy_neighID_array_gpu(networkConst.CellPopulation[0],temp_cellID->data(),temp_cellID->size() * sizeof(uint64_t),GPU_counter);
                temp_cellID->clear();
            }
        }
    }

    //copy to gpu and cpu
    if(Proccesinfo.GPU){
        fn_copy_neighID_array_gpu(networkConst.CellPopulation[GPU_Cell_counter],temp_cellID->data(),temp_cellID->size() * sizeof(uint64_t),GPU_counter);
    }else {
        networkConst.CellPopulation[0].NeighIds = new uint64_t[temp_cellID->size()];
        fn_copy_host_to_dev_cpu(networkConst.CellPopulation[0].NeighIds, temp_cellID->data(), temp_cellID->size() * sizeof(uint64_t));
    }

    //todo pointer fixen
    if(Proccesinfo.GPU) {
        uint64_t  count = 0;
        uint32_t GPU_counter_n = 0;
        uint32_t GPU_Cell_counter_n =  0;
        for (size_t cellid_l = 0; cellid_l < networkConst.nCells_l; cellid_l++) {
            if (cellid_l == networkConst.GPU_numbers[GPU_counter].nCells_gpu + GPU_Cell_counter_n) {
                GPU_counter_n++;
                GPU_Cell_counter_n = cellid_l;
                count = 0;
            }
            networkConst.CellPopulation[cellid_l].NeighIds = &networkConst.CellPopulation[GPU_Cell_counter_n].NeighIds[count];
            count += networkConst.CellPopulation[cellid_l].NeighCount;
        }
    }
    else{
        uint64_t  count = 0;
        for (size_t cellid_l = 0; cellid_l < networkConst.nCells_l; cellid_l++) {
            networkConst.CellPopulation[cellid_l].NeighIds = &networkConst.CellPopulation[0].NeighIds[count];
            count += networkConst.CellPopulation[cellid_l].NeighCount;
        }
    }
    delete temp_cellID;
    return true;
}


//todo -> needs to be better
bool   init_Gap_Connectivity_Matrix_Read_weighted (const char *filename, NetworkConstStruct &networkConst, process_info &Proccesinfo){
    INIT_LOGP(LOG_DEBUG,Proccesinfo.world_rank);

    auto *temp_cells = new Neigh_local_test[networkConst.nCells_l];
    for(size_t cellid_l = 0; cellid_l<networkConst.nCells_l; cellid_l++) {
        temp_cells[cellid_l].NeighIds.reserve(10);  //yeah this should be a better estimate.
        temp_cells[cellid_l].NeigCondu.reserve(10);  //yeah this should be a better estimate.
    }

    log(LOG_DEBUG,0) << "Processing Connectivity Matrix File" << filename << LOG_ENDL;

    std::ifstream infile(filename);
    if (!infile){
        log(LOG_ERR) << " Gap connectivity file not found: " << filename << LOG_ENDL;
        fprintf(stderr, "Gap connectivity file not found %s\n", filename);
        exit(3);
    }

    uint32_t index = 0;
    uint32_t id = 0;
    float weight = 0;
    std::string line;

    while (std::getline(infile, line))
        {
            std::istringstream iss(line);
            if (!(iss >> index >> id >> weight)) { log(LOG_ERR) << "malformed file" << LOG_ENDL;  exit(3); break; } // error
            std::cout << index << ", " << id << ", " << weight << "\n";
            if(id >= networkConst.nCells || index >= networkConst.nCells){
                log(LOG_ERR) << "incorrect network description index: " <<index << "or id: " << id << " is out of bounds" << LOG_ENDL;
                fprintf(stderr, "incorrect network description cell %d %d is out of bounds\n", index,id);
                exit(3);
            }
//            if (index == id) continue; //
            if (index < networkConst.CellID_Offset) continue;
            if ((index - networkConst.CellID_Offset) >= networkConst.nCells_l) continue;
            temp_cells[index-networkConst.CellID_Offset].NeighIds.push_back(id);
            temp_cells[index-networkConst.CellID_Offset].NeigCondu.push_back(weight);
            Proccesinfo.connections_inuse[index] = true; //watch race conditions here.
    }

    uint32_t GPU_counter = 0;
    uint32_t GPU_Cell_counter = 0;
    for(size_t cellid_l = 0; cellid_l<networkConst.nCells_l; cellid_l++) {
        if(Proccesinfo.GPU){
            if(cellid_l == networkConst.GPU_numbers[GPU_counter].nCells_gpu + GPU_Cell_counter){
                GPU_counter++;
                GPU_Cell_counter = cellid_l;
            }
            fn_copy_neighID_array_gpu(networkConst.CellPopulation[cellid_l],temp_cells[cellid_l].NeighIds.data(),temp_cells[cellid_l].NeighIds.size() * sizeof(uint64_t),GPU_counter);
            fn_copy_neighCondu_array_gpu(networkConst.CellPopulation[cellid_l],temp_cells[cellid_l].NeigCondu.data(),temp_cells[cellid_l].NeigCondu.size() * sizeof(float),GPU_counter);
        }else {
            networkConst.CellPopulation[cellid_l].NeighIds = new uint64_t[temp_cells[cellid_l].NeighIds.size()];
            fn_copy_host_to_dev_cpu(networkConst.CellPopulation[cellid_l].NeighIds, temp_cells[cellid_l].NeighIds.data(), temp_cells[cellid_l].NeighIds.size() * sizeof(uint64_t));
            fn_copy_host_to_dev_cpu(networkConst.CellPopulation[cellid_l].NeighCondu, temp_cells[cellid_l].NeigCondu.data(), temp_cells[cellid_l].NeigCondu.size() * sizeof(float));
        }
        networkConst.CellPopulation[cellid_l].NeighCount = temp_cells[cellid_l].NeighIds.size();
        Proccesinfo.total_count += temp_cells[cellid_l].NeighIds.size();
    }

    delete[] temp_cells;
    return true;
}

//todo -> needs to be better
bool init_Gap_Connectivity_Matrix_Read(const char *filename, NetworkConstStruct &networkConst, process_info &Proccesinfo){

    INIT_LOGP(LOG_DEBUG,Proccesinfo.world_rank);

    auto *temp_cells = new Neigh_local_test[networkConst.nCells_l];
    for(size_t cellid_l = 0; cellid_l<networkConst.nCells_l; cellid_l++) {
        temp_cells[cellid_l].NeighIds.reserve(10);  //yeah this should be a better estimate.
    }

    log(LOG_DEBUG,0) << "Processing Connectivity Matrix File" << filename << LOG_ENDL;

    std::ifstream infile(filename);
    if (!infile){
        log(LOG_ERR) << " Gap connectivity file not found: " << filename << LOG_ENDL;
        fprintf(stderr, "Gap connectivity file not found %s\n", filename);
        exit(3);
    }

    uint32_t index = 0;
    uint32_t id = 0;
    uint64_t total_count = 0;

    std::string line;
    while (std::getline(infile, line))
    {
        std::istringstream iss(line);
        if (!(iss >> index >> id)) { log(LOG_ERR) << "malformed file" << LOG_ENDL;  exit(3); break; } // error
        std::cout << index << ", " << id  << "\n";
        if(id >= networkConst.nCells || index >= networkConst.nCells){
            log(LOG_ERR) << "incorrect network description index: " <<index << "or id: " << id << " is out of bounds" << LOG_ENDL;
            fprintf(stderr, "incorrect network description cell %d %d is out of bounds\n", index,id);
            exit(3);
        }
        if (index < networkConst.CellID_Offset) continue; //lower bound
        if ((index - networkConst.CellID_Offset) >= networkConst.nCells_l) continue; //upperbound
        temp_cells[ index-networkConst.CellID_Offset ].NeighIds.push_back(id);
        Proccesinfo.connections_inuse[index] = true; //watch race conditions here.
    }

    uint32_t GPU_counter = 0;
    uint32_t GPU_Cell_counter = 0;
    for(size_t cellid_l = 0; cellid_l<networkConst.nCells_l; cellid_l++) {
        if(Proccesinfo.GPU){
            if(cellid_l == networkConst.GPU_numbers[GPU_counter].nCells_gpu + GPU_Cell_counter){
                std::cout << "no\n";
                GPU_counter++;
                GPU_Cell_counter = cellid_l;
            }
            fn_copy_neighID_array_gpu(networkConst.CellPopulation[cellid_l],temp_cells[cellid_l].NeighIds.data(),temp_cells[cellid_l].NeighIds.size() * sizeof(uint64_t),GPU_counter);
        }else {
            networkConst.CellPopulation[cellid_l].NeighIds = new uint64_t[temp_cells[cellid_l].NeighIds.size()];
            fn_copy_host_to_dev_cpu(networkConst.CellPopulation[cellid_l].NeighIds, temp_cells[cellid_l].NeighIds.data(), temp_cells[cellid_l].NeighIds.size() * sizeof(uint64_t));
        }
        networkConst.CellPopulation[cellid_l].NeighCount = temp_cells[cellid_l].NeighIds.size();
        total_count += temp_cells[cellid_l].NeighIds.size();
    }

    Proccesinfo.total_count = total_count;
    //so now distribute the connections..
    delete[] temp_cells;
    return true;
}


//explain
bool Create_GAP_network(SimRunInfo &sim, NetworkConstStruct &networkConstStruct, process_info &ProcessInfo, RunMetaData* md){

    INIT_LOGP(LOG_DEFAULT, ProcessInfo.world_rank);
    log(LOG_DEBUG) << "starting Gap Generation" << LOG_ENDL;
    md->ConnType = new char[15];

    //allocate space for this mpi thing..
    ProcessInfo.connections_inuse = new bool[networkConstStruct.nCells];

    if(sim.conn_info.type == ConnectivityInfo::NONE) {
        //do nothing because its either not connected or fully connected with constant weights.
        log(LOG_INFO,0) << "FuLLy or NOT connected network and store it in NetworkConst" << LOG_ENDL;
        strncpy(md->ConnType, "None", 15);
        return true;
    }
    else if (sim.conn_info.type == ConnectivityInfo::CONSTANT) {
        //do nothing because its either not connected or fully connected with constant weights.
        log(LOG_INFO,0) << "FuLLy or NOT connected network and store it in NetworkConst" << LOG_ENDL;
        strncpy(md->ConnType, "Constant", 15);
        networkConstStruct.weight = sim.conn_info.constant.weight;
        return true;
    }

    else if(sim.conn_info.type == ConnectivityInfo::RANDOM_BINARY){
        log(LOG_INFO,0) << "Create a RANDOM_BINARY connected network and store it in NetworkConst no custom Weight supported !" << LOG_ENDL;
        fn_random_binary_gap_generation(sim.conn_info.random_binary.density,sim.conn_info.random_binary.random_seed,networkConstStruct,ProcessInfo);
        md->target_density = sim.conn_info.random_binary.density;
        md->actual_density = (ProcessInfo.total_count / networkConstStruct.nCells_l);
        networkConstStruct.weight = sim.conn_info.random_binary.weight;
        strncpy(md->ConnType, "Random_binary", 15);
        return true;
    }

    else if(sim.conn_info.type == ConnectivityInfo::GAUSSIAN){
        log(LOG_INFO,0) << "Create a GAUSSIAN connected network and store it in NetworkConst" << LOG_ENDL;
        fn_gaussian_gap_generation(sim.conn_info.gaussian.density, sim.conn_info.gaussian.mean, sim.conn_info.gaussian.variance, sim.conn_info.gaussian.random_seed, networkConstStruct, ProcessInfo);
        md->target_density = sim.conn_info.gaussian.density;
        md->actual_density = (ProcessInfo.total_count / networkConstStruct.nCells_l);
        strncpy(md->ConnType, "Gaussian", 15);
        networkConstStruct.weight = sim.conn_info.gaussian.weight;
        return true;
    }

    else if(sim.conn_info.type == ConnectivityInfo::READ){
        log(LOG_INFO,0) << "Create a READ connected network and store it in NetworkConst" << LOG_ENDL;
        networkConstStruct.weight = sim.conn_info.read.weight;
        if(sim.conn_info.use_condu){
            init_Gap_Connectivity_Matrix_Read_weighted(sim.conn_info.read.filename, networkConstStruct, ProcessInfo); //read file with no weights
        }
        else{
            init_Gap_Connectivity_Matrix_Read(sim.conn_info.read.filename, networkConstStruct, ProcessInfo); //read file with no weights
        }
        return true;
    }
    else{
        log(LOG_ERR) << "unsupported connectivity info upto now program is not gonna work" << LOG_ENDL;
        exit(0);
    }
}
