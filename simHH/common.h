//
// Created by max on 22-03-21.
//
#ifndef MGPUHH_V2_COMMON_H
#define MGPUHH_V2_COMMON_H


#include "miniLogger/miniLogger.h"
#include <chrono>
#include <thread> //please do this differently
#include <cstring>
//for memory usage measurement


#ifdef _WIN32
#include <limits.h>
#include <intrin.h>
typedef unsigned __int32  uint32_t;
#else
#include <cstdint>
#include "sys/types.h"
#include "sys/sysinfo.h"
#endif

//settings
//LOG_WARN  -- quite terminal
//LOG_MES   -- default
//LOG_TIME  -- progress updates and time update in the end
//LOG_INFO  -- INFO
#define LOG_DEFAULT LOG_MES

#define ORDEREDBLOCK 0
#define BLOCKSIZE  32

//gpu specific
#define GAP_THREADS_PER_BLOCK  32
#define GAP_THREADS_PER_CELL  1

//MACROS for easy programming :D
#define minSS(a,b) a<b ? a: b;
#define mask1 3         // 0011
#define mask2 12        // 1100

//memory measurments

// A very fast and chaotic RNG
// straight from Wikipedia nice !
class XorShiftMul{
private:
    uint64_t state[1];
public:
    // NB if xorshiftmul's state becomes 0 it outputs zero forever :D
    // force it to not happen, with the highest bit set
    // In this case, certain combinations of shift factors ensure a complete cycle through all non-zero values.
    // It has been proven through linear algebra, somehow.
    explicit XorShiftMul(uint64_t _seed):
     state{_seed | (1ULL << 63)}
     {
         sample_scale   = (1 << 24); //how many bits wide should a random sample be?
         max_rand_value = 16777215;  //maximum rand value

     };
    void update(uint64_t _seed){state[0] = _seed | (1ULL << 63);}

    uint32_t sample_scale;
    uint32_t max_rand_value;

    float GetF(){
        uint64_t temp = Get() % sample_scale;
        float rvalue =((float) temp / (float) (max_rand_value));
        return rvalue;
    }

    uint64_t Get(){
        uint64_t x = state[0];
        x ^= x >> 12; // a
        x ^= x << 25; // b
        x ^= x >> 27; // c
        state[0] = x;
        return x * 0x2545F4914F6CDD1D;
    }

};

//The most minimal representation of the network in terms of bytes and easy access (I hope/wish)
typedef struct{
    float p;
    float aX[9];
    float bX[9];
    uint32_t aFtype;            //optioneel samen voegen
    uint32_t bFtype;            //optioneel samen voegen
} GateConstStruct;           //84  bytes (64 bit system)
typedef struct {
    bool is_ca2plus_channel;
    uint32_t nGates;
    GateConstStruct *Gate;
} ChannelConstStruct;        //13  bytes (64 bit system)
typedef struct {
    float S;      //inverse capacitance of compartment
    float vLeak;  //voltage baseline of passive leak
    float gLeak;  //conductivity of passive channel
    float gInt;   // internal conductance of the whole cell. (same for each compartment)
    float gp;     // ratio p or g_int ie an inverse scaling factor to adjust intercompartmental conductivity

    uint32_t identifier;                   //To identify first compartment
    uint32_t nChannels;
    ChannelConstStruct *Channel;
    GateConstStruct CalciumConcentration;  //Calcium concentration support                //84 bytes
    uint32_t enable_CalciumConcentration;  //Cannot be a bool because of allignment       //4  bytes
} CompartmentConstStruct;    //124 bytes (64 bit system)
typedef struct  {
    uint32_t  NeighCount;                    //4
    uint64_t *NeighIds;                      //16
    float    *NeighCondu;                    //4
    CompartmentConstStruct *compartment;     //8
} CellConstStruct;           //24  bytes (64 bit system)
typedef struct  {
    uint32_t nCells_gpu;                //gpu !
    uint32_t nCompartments_gpu;         //gpu !
    uint32_t nChannels_gpu;             //gpu !
    uint32_t nGates_gpu;                //gpu !
    uint32_t CellID_Offset_gpu;
} GPU_number_obj;           //24  bytes (64 bit system)
typedef struct {
    uint32_t CellID_Offset;          //offset to globalID

    uint64_t nCells;
    uint32_t nCells_l;                //local !
    uint32_t nCompartments_l;         //local !
    uint32_t nChannels_l;             //local !
    uint32_t nGates_l;                //local !

    uint32_t nCells_u;                //local !
    uint32_t nCompartments_u;         //local !
    uint32_t nChannels_u;             //local !
    uint32_t nGates_u;                //local !

    GPU_number_obj *GPU_numbers;

    float  weight;

    //this maps the compID to a specific unique index
    uint32_t *CompartmentIndex;      //To match compID to Compartment index
    uint32_t *GateNO_vs_GateIndex;   //To match gateID to gateINDEX

    //this are how the indexes should work
    uint32_t *CellID_match_compID;   //To match compID to Cell_ID
    uint32_t *ChannelIndex;          //To match compID to Channel index
    uint32_t *GateIndex;             //To match compID to gate index
    uint32_t *Comp_index_vs_gates;   //To match gateID to compID


    //Big arrays for randimization support!
    float *G_int;                   //support randomization of the G_int ?
    float *G_Channels;              //Support randomization on the Conduction variable of the channels
    float *Inverpotential_Channels; //Support randomization on the inverspotentials variable of the channels.  YES THERE IS A TYPO _._
    float *PassiveLeakConductivity; //Support randomization of PassiveLeakConductivity

    CellConstStruct *CellPopulation;

    //needed to remember the arrays
    CellConstStruct *CellPop;
    CompartmentConstStruct *compartment;
    ChannelConstStruct *Channel;
    GateConstStruct *Gate;
} NetworkConstStruct;        //32 ish

//The network State
struct Network_state {
    //Double Buffering the network state
    float *vsNow;        //current compartment Potentials
    float *ysNow;        //current activation variables
    float *calcNow;      //current Calcium concentrations
    float *currentsNow;  //placeholder when Writing Channel currents is enabled!
    float *vsNext;       //Next compartment Potentials
    float *ysNext;       //Next activation variables
    float *calcNext;     //Next Calcium concentrations
    float *currentsNext; //placeholder when Writing Channel currents is enabled!

    //Buffer to save and print host side
    float *hostVs;        //CPU residing buffer for Compartent potentials
    float *hostYs;        //CPU residing buffer for activation variables
    float *hostCalc;            //CPU residing buffer for Calcium Concentration
    float *hostCurrents;        //CPU residing buffer for Channel Currents
    float *hostVs_print;        //CPU residing buffer for Compartent potentials
    float *hostYs_print;        //CPU residing buffer for activation variables
    float *hostCalc_print;      //CPU residing buffer for Calcium Concentration
    float *hostCurrents_print;  //CPU residing buffer for Channel Currents

    //Igap to copy between kernels
    float *Igap;          //To communicated gap current from IGAP_calc kernel to COMP_UPDATE kernel
    float *Igap_host;     //host buffer  //todo

    //For better accessing the gap junctions potentials (we only support gap junctions to comparment 1
    float *VS_GAP;        //Duplicate of VSNOW with only gap specific compartment Potentials
    float *VS_GAP_N;      //Duplicate of VSNOW with only gap specific compartment Potentials Double buffer to stay away from race conditions in multi gpu setup
    float *VS_GAP_host;   //host buffer //todo

    //for ou_noise
    float *ou_noise;   //OrnsteinUhlenbecknoise support !
    void  *rand_state;
    XorShiftMul* rng;
};

//process informations
struct process_info
{
    //general process info
    int world_rank;
    int world_size;
    int world_network_size;
    char *processor_name;
    bool MPI;
    int GPU;
    bool MPI_alltoallv_enable;
    bool OrderedBlock;
    int MPThreads;

    //cpu backend

    //gpu backend.
    bool MonitorGPUkernels;
    void *GPUstreams;
    void *GPUevents;

    //mpi communicator
    int *sdispls;                                                             // Send displacement MPI
    int *rdispls;                                                             // receive displacement MPI
    int *cellstoSend;                                                         // receive displacement MPI
    int *xcell_count_pp;                                                      // Count of xternal cells per process
    int *xcell_count_pp_g;                                                    // Count of xternal cells per process global
    int *xcell;                                                               // Array with xertnal cell IDs
    int xcell_size_global;                                                    // Total xternal count global
    int xcell_size_local;                                                     // Total xternal count local
    bool *connections_inuse;                                                  // map the in use connections for this node
    uint64_t total_count;
    float *VtoSend_MPI;
    float *VtoStore_MPI;

    explicit process_info(int name_lenght){
        GPUstreams = nullptr;
        GPU = 0;
        MonitorGPUkernels = true;
        world_rank = 0;
        world_size = 0;
        world_network_size = 0;
        processor_name = new char[name_lenght];
        MPI = false;
        MPI_alltoallv_enable = false;

        sdispls = nullptr;                                                                    // Send displacement MPI
        rdispls= nullptr;                                                                    // receive displacement MPI
        cellstoSend = nullptr;                                                         // receive displacement MPI
        xcell_count_pp = nullptr;                                                      // Count of xternal cells per process
        xcell_count_pp_g = nullptr;                                                    // Count of xternal cells per process global
        xcell = nullptr;                                                               // Array with xertnal cell IDs
        xcell_size_global = 0;                                                       // Total xternal count global
        xcell_size_local = 0;                                                        // Total xternal count local
        VtoSend_MPI = nullptr;
        VtoStore_MPI = nullptr;
        OrderedBlock = false;
        MPThreads = 0;
        total_count = 0;
    }
};

//A basic timer
typedef double timestamp_t;
struct Timer
{
    std::chrono::time_point<std::chrono::high_resolution_clock> _start, _end;
    std::chrono::duration<timestamp_t> _duration{};
    Timer()
    {
        _start = std::chrono::high_resolution_clock::now();
    }
    timestamp_t get_time(){
        _end = std::chrono::high_resolution_clock::now();
        _duration = _end - _start;
        return _duration.count() * 1000.0f;  //in ms
    }
     ~Timer() = default;
};
static inline void native_cpuid(unsigned int *eax, unsigned int *ebx,unsigned int *ecx, unsigned int *edx)
{
#if defined(unix) || defined(__unix__) || defined(__unix)
    /* ecx is often an input as well as an output. */
    asm volatile("cpuid" :
        "=a" (*eax),
        "=b" (*ebx),
        "=c" (*ecx),
        "=d" (*edx)
       : "0" (*eax), "2" (*ecx)
       : "memory"
      );

#else
    //i have no clue how to do this on windows // mac should be oke here !
#endif
}
struct sGPU_Meta_s{
    char* name;
    int clockRate;
    int major;
    int minor;

    size_t memsizeused;
    size_t totalGlobalMem;

    timestamp_t time_compute;
    timestamp_t time_Igap;
    timestamp_t time_Icalc_comp;
    timestamp_t time_Icalc_gate;
    timestamp_t time_dev_copy;
    timestamp_t time_p2p_copy;

    sGPU_Meta_s(){
        name = nullptr;
        clockRate = 0;
        major = 0;
        minor = 0;

        memsizeused = 0;
        totalGlobalMem = 0;

        time_compute = 0.0;
        time_Igap = 0.0;
        time_Icalc_comp = 00.;
        time_Icalc_gate = 0.0;
        time_dev_copy = 0.0;
        time_p2p_copy = 0.0;
    }

    void print(miniLogger &log, int i){
        if(name) {
            log(LOG_TIME) << "  - GPU(" << i << ") " << name << "\t\t comc:" << major << "." << minor << LOG_ENDL;
//            log(LOG_TIME) << "      Computetime --todo? (s):  " << time_compute / 1000 << LOG_ENDL;
            log(LOG_TIME) << "    - MEMusage   (MB):  " << (int) memsizeused / 1000000 << "/" << (int) totalGlobalMem / 1000000 << LOG_ENDL;
            log(LOG_TIME) << "    - kernel times (ms)->" << LOG_ENDL;
            log(LOG_TIME) << "      - Igap:           " << time_Igap  << LOG_ENDL;
            log(LOG_TIME) << "      - Icalc_comp:     " << time_Icalc_comp  << LOG_ENDL;
            log(LOG_TIME) << "      - Icalc_gate:     " << time_Icalc_gate  << LOG_ENDL;
            log(LOG_TIME) << "      - time_p2p_copy:  " << time_p2p_copy  << LOG_ENDL;
            log(LOG_TIME) << "      - time_dev_copy:  " << time_dev_copy  << LOG_ENDL;
        }else{
            log(LOG_ERR) << "GPU data not set" << LOG_ENDL;
        }
    }

};
struct RunMetaData{
    //some details
    char* Proces_name;

    //network information
    uint32_t Steps;
    uint32_t nCells;
    uint32_t nCells_l;
    uint32_t nCompartments_l;
    uint32_t nChannels_l;
    uint32_t nGates_l;
    uint32_t nCells_u;
    uint32_t nCompartments_u;
    uint32_t nChannels_u;
    uint32_t nGates_u;
    uint64_t NOConnections;
    uint32_t NoXternalConnections;
    uint32_t target_density;
    uint32_t actual_density;
    char* ConnType;


    //MPI
    int NoMPIprocesses;
    int NoMPIprocess;
    bool Alltoallv;
    int xcell_size_global;                                                    // Total xternal count global
    int xcell_size_local;                                                     // Total xternal count local
    int *xcell_count_pp;                                                      // Count of xternal cells per process
    int *xcell_count_pp_g;                                                    // Count of xternal cells per process global

    //some timers
    timestamp_t SimulationTimeFull_ms;
    timestamp_t NetworkSynapticNetworkGenerationTime_ms;
    timestamp_t Networkinitializations_ms;
    timestamp_t Timsteps_ms;
    timestamp_t OutputWriteTime_ms;
    timestamp_t WaitForOutputThread_ms;
    timestamp_t ComputeLaunchTime_ms;
    timestamp_t MPICommunicationTime_ms;
    timestamp_t LaunchWriteThread_ms;
    timestamp_t Synchronizingtheloop1_ms;
    timestamp_t Synchronizingtheloop2_ms;

    RunMetaData():
        Proces_name(nullptr), //
        Steps(0),             //
        nCells(0),            //
        nCells_l(0),          //
        nCompartments_l(0),
        nChannels_l(0),
        nGates_l(0),
        nCells_u(0),
        nCompartments_u(0),
        nChannels_u(0),
        nGates_u(0),
        NOConnections(0),
        NoXternalConnections(0),
        target_density(0),
        actual_density(0),
        ConnType(nullptr),
        NoMPIprocesses(0),
        NoMPIprocess(0),
        Alltoallv(false),
        xcell_size_global(0),
        xcell_size_local(0),
        xcell_count_pp(nullptr),
        xcell_count_pp_g(nullptr),
        SimulationTimeFull_ms(0.0),
        NetworkSynapticNetworkGenerationTime_ms(0.0),
        Networkinitializations_ms(0.0),
        Timsteps_ms(0.0),
        OutputWriteTime_ms(0.0),
        WaitForOutputThread_ms(0.0),
        ComputeLaunchTime_ms(0.0),
        MPICommunicationTime_ms(0.0),
        LaunchWriteThread_ms(0.0),
        Synchronizingtheloop1_ms(0.0),
        Synchronizingtheloop2_ms(0.0),
        type(UNSET)
    {}

    enum{
        UNSET,
        CPU,
        GPU
    }type;

    struct _metaCPU{
        char *name;
        uint NumThreads;
        timestamp_t Computetime_ms;
        int64_t peak_resident_memory_bytes;
        int64_t current_resident_memory_bytes;

        _metaCPU():
          name(nullptr),
          NumThreads(std::thread::hardware_concurrency()),
          Computetime_ms(0.0),
          peak_resident_memory_bytes(0),
          current_resident_memory_bytes(0)
        {
            //todo! not safe should check first if possible.
            uint32_t eax = 0, ebx = 0, ecx = 0, edx = 0;
            eax = 0x80000002;
            native_cpuid(&eax, &ebx, &ecx, &edx);
            char vendor[49];

            memcpy(vendor, &eax, 4);
            memcpy(vendor+4, &ebx, 4);
            memcpy(vendor+8, &ecx, 4);
            memcpy(vendor+12, &edx, 4);

            eax = 0x80000003;
            native_cpuid(&eax, &ebx, &ecx, &edx);
            memcpy(vendor+16, &eax, 4);
            memcpy(vendor+20, &ebx, 4);
            memcpy(vendor+24, &ecx, 4);
            memcpy(vendor+28, &edx, 4);


            eax = 0x80000004;
            native_cpuid(&eax, &ebx, &ecx, &edx);
            memcpy(vendor+32, &eax, 4);
            memcpy(vendor+36, &ebx, 4);
            memcpy(vendor+40, &ecx, 4);
            memcpy(vendor+44, &edx, 4);

            vendor[48] = '\0';
            name = new char[49];
            memcpy(name, vendor, 48);
        }
    };
    struct _metaGPU{
        uint NumGPUS;
        sGPU_Meta_s *sGPU_Meta;

        _metaGPU():
          NumGPUS(0),
          sGPU_Meta(nullptr)

        {}
    };

    _metaCPU metaCPU;
    _metaGPU metaGPU;

    void init(NetworkConstStruct *Pros){
        nCells          = Pros->nCells;
        nCells_l        = Pros->nCells_l;
        nCompartments_l = Pros->nCompartments_l;
        nChannels_l     = Pros->nChannels_l;
        nGates_l        = Pros->nGates_l;
        nCells_u        = Pros->nCells_u;
        nCompartments_u = Pros->nCompartments_u;
        nChannels_u     = Pros->nChannels_u;
        nGates_u        = Pros->nGates_u;
    }
    void initMPI(process_info &Pros){
        NoMPIprocesses = Pros.world_size;
        NoMPIprocess   = Pros.world_rank;
        Alltoallv      = Pros.MPI_alltoallv_enable;
        xcell_size_global = Pros.xcell_size_global;
        xcell_size_local = Pros.xcell_size_local;
        xcell_count_pp   = Pros.xcell_count_pp;
        xcell_count_pp_g = Pros.xcell_count_pp_g;
    }

    void print_debug(miniLogger &log) const{
                            log(LOG_TIME) << "Name:                               " << Proces_name          << LOG_ENDL;
                            log(LOG_TIME) << "CPU:                                " << metaCPU.name         << LOG_ENDL;
                            log(LOG_TIME) << "SimulationTime_ms                   " << SimulationTimeFull_ms << LOG_ENDL;
                            log(LOG_TIME) << " - SynapticNetworkGenerationTime_ms " << NetworkSynapticNetworkGenerationTime_ms << LOG_ENDL;
                            log(LOG_TIME) << " - Networkinitializations_ms        " << Networkinitializations_ms << LOG_ENDL;
                            log(LOG_TIME) << " - Simulation_runtime_ms            " << Timsteps_ms << LOG_ENDL;
                            log(LOG_TIME) << "Simulation_runtime(" << Timsteps_ms << "):" << LOG_ENDL;
                            log(LOG_TIME) << " - LaunchWriteThread_ms             " << LaunchWriteThread_ms << LOG_ENDL;
                            log(LOG_TIME) << " - OutputWriteTime_ms               " << OutputWriteTime_ms << LOG_ENDL;
                            log(LOG_TIME) << " - WaitForOutputThread_ms           " << WaitForOutputThread_ms << LOG_ENDL;
        if(type == GPU)     log(LOG_TIME) << " - ComputeLaunchTime_ms             " << ComputeLaunchTime_ms << LOG_ENDL;
        if(type == GPU)     log(LOG_TIME) << " - Synchronizingtheloop1_ms         " << Synchronizingtheloop1_ms << LOG_ENDL;
        if(NoMPIprocesses>1)log(LOG_TIME) << " - MPICommunicationTime_ms          " << MPICommunicationTime_ms << LOG_ENDL;
        if(type == GPU)     log(LOG_TIME) << " - Synchronizingtheloop2_ms         " << Synchronizingtheloop2_ms << LOG_ENDL;
        if(type == CPU){
            log(LOG_TIME) << "CPU backend is selected ->"                       << LOG_ENDL;
            log(LOG_TIME) << " - Number of Threads:               " << metaCPU.NumThreads << LOG_ENDL;
            log(LOG_TIME) << " - Computetime_ms:                  " << metaCPU.Computetime_ms << LOG_ENDL;
            log(LOG_TIME) << " - peak_memory_bytes (MB):          " << metaCPU.peak_resident_memory_bytes/1000 << LOG_ENDL;
            log(LOG_TIME) << " - current_memory_bytes (MB):       " << metaCPU.current_resident_memory_bytes/1000 << LOG_ENDL;
        }
        if(type == GPU){
            log(LOG_TIME) << "GPU backend is selected:"                    << LOG_ENDL;
            for(size_t i = 0; i < metaGPU.NumGPUS; i++) {
                metaGPU.sGPU_Meta[i].print(log, i);
            }
        }
    }
};

#endif //MGPUHH_V2_COMMON_H
