//
// Created by max on 24-03-21.
//

#ifndef MGPUHH_V2_GPU_KERNELS_H
#define MGPUHH_V2_GPU_KERNELS_H

#include "common.h"
#include "Modular_sim_object.h"

bool fn_copy_vsgap_to_host_gpu(float *VS_GAP_host, float *VS_GAP_dev, uint32_t size, uint32_t offset,process_info *ProcessInfo);
bool fn_copy_vsgap_to_device_gpu(float *VS_GAP_host, float *VS_GAP_dev, uint32_t size,process_info *ProcessInfo);
Network_state * fn_init_gpu_Backend_state(SimRunInfo &sim,process_info &ProcessInfo,Network_state *NetworkState_l,NetworkConstStruct *NetworkConst_l);
NetworkConstStruct* fn_init_gpu_Backend_const(SimRunInfo &sim,process_info &ProcessInfo,Network_state *NetworkState_l,NetworkConstStruct *NetworkConst_l);
void fn_init_gpu_Backend_streams(process_info *ProcessInfo,RunMetaData &runMetaData); //must be last init step
bool sync_process_stream(process_info &ProcessInfo, int process);
bool setdevice(int ID);

bool RunTimestep_gpu(size_t step, SimRunInfo &sim, Network_state *NetworkState_l,NetworkConstStruct *NetworkConst_l, process_info &ProcessInfo);
bool fn_copy_back_dev_to_host_gpu(SimRunInfo &sim, Network_state &NetworkState_l,NetworkConstStruct &NetworkConst_l, GPU_number_obj &GPUno);
void fn_copy_host_to_dev_gpu(void* dev, const void* host, size_t size);
void fn_copy_neighID_array_gpu(CellConstStruct &CellPopulation, void *host, size_t size,int GPU_ID);
void fn_copy_neighCondu_array_gpu(CellConstStruct &CellPopulation, void *host, size_t size,int GPU_ID);

bool GPU_checker(SimRunInfo &sim,process_info &ProcessInfo,RunMetaData &runMetaData);
bool updatemetadataGPU(process_info &ProcessInfo, RunMetaData &md);
bool share_betweenGPUS_aftermpi(process_info *ProcessInfo,Network_state *NetworkState_l,NetworkConstStruct *NetworkConst_l);

bool testIEKS();
#endif //MGPUHH_V2_GPU_KERNELS_H
