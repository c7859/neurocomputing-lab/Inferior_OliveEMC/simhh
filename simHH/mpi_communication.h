//
// Created by max on 03-05-21.
//

#ifndef SIMHH_MPI_COMMUNICATION_H
#define SIMHH_MPI_COMMUNICATION_H
#include "common.h"
#ifdef USE_MPI
#include <mpi.h>        //mpi support
#define MPI_CHECK_RETURN(error_code) {                                           \
    if (error_code != MPI_SUCCESS) {                                             \
        char error_string[BUFSIZ];                                               \
        int length_of_error_string;                                              \
        int world_rank;                                                          \
        MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);                              \
        MPI_Error_string(error_code, error_string, &length_of_error_string);     \
        fprintf(stderr, "%3d: %s\n", world_rank, error_string);                  \
        exit(1);                                                                 \
    }}
#endif

bool mpi_communicator_init(Network_state* NetworkState_l, NetworkConstStruct* NetworkConst_l, process_info& ProcessInfo);
bool mpi_communicator_timestep(Network_state* NetworkState_l, NetworkConstStruct* NetworkConst_l, process_info* ProcessInfo);

#endif //SIMHH_MPI_COMMUNICATION_H
