//
// Created by max on 24-03-21.
//

#ifndef MGPUHH_V2_SIMULATION_H
#define MGPUHH_V2_SIMULATION_H

#include "common.h"
#include "Modular_sim_object.h"

//settings


//function
bool runSimulation(SimRunInfo &sim,process_info &ProcessInfo,Network_state *NetworkState_l,NetworkConstStruct *NetworkConst_l,RunMetaData &runMetaData);

#endif //MGPUHH_V2_SIMULATION_H
