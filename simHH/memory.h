//
// Created by max on 07-05-21.
//

#ifndef SIMHH_MEMORY_H
#define SIMHH_MEMORY_H
#include "stdlib.h"

size_t get_memory_total_cpu();
size_t get_memory_cpu_current();
#endif //SIMHH_MEMORY_H
