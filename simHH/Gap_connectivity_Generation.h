//
// Created by max on 02-04-21.
//

#ifndef MGPUHH_V2_GAP_CONNECTIVITY_GENERATION_H
#define MGPUHH_V2_GAP_CONNECTIVITY_GENERATION_H

#include "common.h"
#include "Modular_sim_object.h"

struct Neigh_local_test{
    std::vector<uint64_t> NeighIds;
    std::vector<uint64_t> NeigCondu;
};

bool Create_GAP_network(SimRunInfo &sim, NetworkConstStruct &networkConstStruct, process_info &ProcessInfo,RunMetaData* md);

#endif //MGPUHH_V2_GAP_CONNECTIVITY_GENERATION_H
