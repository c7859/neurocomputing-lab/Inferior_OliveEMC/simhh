#include "common.h"
#include "Modular_sim_object.h"
#include "JSON.h"
#include "initializations.h"
#include "Simulation.h"
#include "Gap_connectivity_Generation.h"
#include "GPU_kernels.h"
#include <omp.h>
#include <cstring>        // std::memcpys
#include <cstdlib>

#ifdef USE_MPI
#include <mpi.h>        //mpi support
#define MPI_CHECK_RETURN(error_code) {                                           \
    if (error_code != MPI_SUCCESS) {                                             \
        char error_string[BUFSIZ];                                               \
        int length_of_error_string;                                              \
        int world_rank;                                                          \
        MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);                              \
        MPI_Error_string(error_code, error_string, &length_of_error_string);     \
        fprintf(stderr, "%3d: %s\n", world_rank, error_string);                  \
        exit(1);                                                                 \
    }}
#endif

bool simHH(const char *filename){
    Timer simHH_timer;

    //The variables we need to track
    process_info ProcessInfo(250);
    RunMetaData  MetaData;
    INIT_LOGP(LOG_DEFAULT,ProcessInfo.world_rank);

    //create the process info
#ifdef USE_MPI
    ProcessInfo.MPI = true;
    int test = 0;
    MPI_CHECK_RETURN(MPI_Init_thread(NULL, NULL,MPI_THREAD_FUNNELED, &test));
    if(test != MPI_THREAD_FUNNELED){
        log(LOG_ERR) << "Somethings is wrong with the mpi init " << test << " != MPI_THREAD_FUNNELED (1)" << LOG_ENDL;
    }
    MPI_CHECK_RETURN(MPI_Comm_size(MPI_COMM_WORLD, &ProcessInfo.world_size));
    MPI_CHECK_RETURN(MPI_Comm_rank(MPI_COMM_WORLD, &ProcessInfo.world_rank));
    log.set_processID(ProcessInfo.world_rank);
    int name_len;
    MPI_CHECK_RETURN(MPI_Get_processor_name(ProcessInfo.processor_name, &name_len));
    log.mpi();
    log(LOG_INFO)<< ProcessInfo.processor_name << " reports for duty, rank "<<ProcessInfo.world_rank<<" out of "<<ProcessInfo.world_size<<" processors" << LOG_ENDL;
    MPI_CHECK_RETURN(MPI_Barrier(MPI_COMM_WORLD));
    MetaData.NoMPIprocess   = ProcessInfo.world_rank;
    MetaData.NoMPIprocesses = ProcessInfo.world_size;
    MetaData.Proces_name = new char[name_len];
    strncpy(MetaData.Proces_name, ProcessInfo.processor_name, name_len);
#else
    ProcessInfo.world_size  = 1;
    ProcessInfo.world_rank  = 0;
    MetaData.NoMPIprocess   = 0;
    MetaData.NoMPIprocesses = 1;
    MetaData.Proces_name = new char[256];
    strncpy(MetaData.Proces_name, "No MPI is used, so just your computer", 256);
#endif
    //welkom message
    log(LOG_MES,0) << "Hello, World! this is simHH Version 0.1" << LOG_ENDL;
#ifdef USE_CUDA
    log(LOG_DEBUG,0) << "CUDA available" << LOG_ENDL;
#endif
#ifdef USE_MPI
    log(LOG_DEBUG,0) << "MPI available" << LOG_ENDL;
#endif

    //preferably keep these on the stack
    SimRunInfo sim(LOG_DEFAULT,ProcessInfo.world_rank);

    auto *NetworkState_l = new Network_state[1];
    auto *NetworkConst_l = new NetworkConstStruct[1];

    //create the simulation object
    log(LOG_INFO,0) << "-->Create the simulation object" << LOG_ENDL;
    ReadSimRunDefinition(filename,sim,ProcessInfo);
    sim.output_monitor.OpenFiles(ProcessInfo.world_rank,ProcessInfo.MPI);
    ProcessInfo.GPU = sim.GPU_backend;
    ProcessInfo.MPI_alltoallv_enable = sim.MPI_alltoallv_enable;
    ProcessInfo.MPThreads = sim.MPThreads;
    ProcessInfo.MonitorGPUkernels = sim.GPU_Time_kernels;
    if(ProcessInfo.MPThreads > (int)MetaData.metaCPU.NumThreads){
        log(LOG_ERR) << "More threads requested then available" << LOG_ENDL;
        exit(0);
    }
    else if(ProcessInfo.MPThreads){
        omp_set_dynamic(0);     // Explicitly disable dynamic teams
        omp_set_num_threads(ProcessInfo.MPThreads); // Use 4 threads for all consecutive parallel regions
        log(LOG_INFO) << "Disable dynamic teams for openMP" << LOG_ENDL;
        MetaData.metaCPU.NumThreads = ProcessInfo.MPThreads;
    }else{
        omp_set_dynamic(1);     // Explicitly enable dynamic teams
        log(LOG_INFO) << "Enable dynamic teams for openMP" << LOG_ENDL;
    }

    ProcessInfo.OrderedBlock = sim.OrderedBlock;
    if(ProcessInfo.OrderedBlock != ORDEREDBLOCK){
        if(ORDEREDBLOCK){log(LOG_ERR) << "Compiled with orderedblocks on" << LOG_ENDL;}
        else{log(LOG_ERR) << "Compiled with orderedblocks off" << LOG_ENDL;}
        exit(2);
    }
    if(sim.GPU_backend){
        MetaData.type = RunMetaData::GPU;
        log(LOG_INFO) << "gpu backend" << LOG_ENDL;
        GPU_checker(sim,ProcessInfo,MetaData); //-->> check if it's possible
    }else{
        MetaData.type = RunMetaData::CPU;
    }
    //Create the balancer atm very dump code which spreads evenly over all nodes! and initialize the calculation structures
    log(LOG_INFO,0) << "-->Initialize the simulation object" << LOG_ENDL;
    Initialize_Balanced_constant_state(sim,ProcessInfo,*NetworkConst_l,*NetworkState_l);

    //TODO create the GapJunction Network
    {
        Timer gapT;
        log(LOG_INFO, 0) << "-->Generate The Gap junction network" << LOG_ENDL;
        Create_GAP_network(sim, *NetworkConst_l, ProcessInfo,&MetaData);
        MetaData.NetworkSynapticNetworkGenerationTime_ms = gapT.get_time();
        MetaData.NOConnections = ProcessInfo.total_count;
        if(MetaData.target_density != 0) {
            if(abs(int(MetaData.actual_density - MetaData.target_density))>10){
                log(LOG_ERR) << "percentage_diff is to high between: " <<  abs(int(MetaData.actual_density - MetaData.target_density))<<  LOG_ENDL;
            }
        }

    }

#ifdef USE_MPI
    MPI_CHECK_RETURN(MPI_Barrier(MPI_COMM_WORLD));
#endif

    //RunSimulation
    MetaData.Networkinitializations_ms += simHH_timer.get_time();
    log(LOG_MES,0) << "progress = " << "0%" << "\t\t Initializing took: " << (size_t)(simHH_timer.get_time()/1000) << " s" << LOG_ENDL;
    log(LOG_INFO,0) << "-->Run the simulation" << LOG_ENDL;
    runSimulation(sim,ProcessInfo,NetworkState_l,NetworkConst_l,MetaData);

    //Finish and clean up simulation
    log(LOG_INFO,0) << "-->Finish up simulation" << LOG_ENDL;
    sim.output_monitor.CloseFiles();
    MetaData.SimulationTimeFull_ms = simHH_timer.get_time();

#ifdef USE_MPI
    for(int count = 0; count < ProcessInfo.world_size; count++){
        if(ProcessInfo.world_rank == count)
            MetaData.print_debug(log);
        MPI_CHECK_RETURN(MPI_Barrier(MPI_COMM_WORLD));
    }
    MPI_Finalize();
#else
    MetaData.print_debug(log);
#endif

    sim.meta_data_monitor.open_file(ProcessInfo.world_rank,ProcessInfo.MPI);
    if(!WriteMetaData_json(sim.meta_data_monitor.MetaData_f,MetaData)){
        log(LOG_ERR) << "Could not save metadata to file: "<< sim.meta_data_monitor.name <<LOG_ENDL;
    }
    sim.meta_data_monitor.CloseFiles();
    log(LOG_INFO,0) << "-->Terminate program" << LOG_ENDL;
    return false;
}
