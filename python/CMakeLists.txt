#add check if excist aka submodule !!

set(pb11_src_dir "${PROJECT_SOURCE_DIR}/python/pybind11")
add_subdirectory(pybind11)

set(python_sources
        main.cpp
    )

# C++ and pybind accessible
add_library(pysimHH_obj OBJECT ${python_sources})
target_link_libraries(pysimHH_obj PRIVATE simHH pybind11::module)
set_property(TARGET pysimHH_obj PROPERTY POSITION_INDEPENDENT_CODE TRUE)


# The Python library. MODULE will make a Python-exclusive model.
add_library(pysimHH MODULE $<TARGET_OBJECTS:pysimHH_obj>)
set_target_properties(pysimHH PROPERTIES PREFIX "${PYTHON_MODULE_PREFIX}" SUFFIX "${PYTHON_MODULE_EXTENSION}")
set_target_properties(pysimHH PROPERTIES OUTPUT_NAME pysimHH)
set(python_mod_path "${CMAKE_CURRENT_BINARY_DIR}/pysimHH")
set_target_properties(pysimHH PROPERTIES LIBRARY_OUTPUT_DIRECTORY "${python_mod_path}")

target_link_libraries(pysimHH PRIVATE simHH pybind11::module)


#van arbor gejat
execute_process(
        COMMAND ${PYTHON_EXECUTABLE} -c
        "import sys,sysconfig;pfx=sys.stdin.read();print(sysconfig.get_path('platlib',vars={} if pfx=='' else {'base':pfx,'platbase':pfx}))"
        INPUT_FILE "${CMAKE_BINARY_DIR}/install-prefix"
        OUTPUT_VARIABLE ARB_PYTHON_LIB_PATH_DEFAULT
        OUTPUT_STRIP_TRAILING_WHITESPACE)

# Default to installing in that path, override with user specified ARB_PYTHON_LIB_PATH
set(ARB_PYTHON_LIB_PATH ${ARB_PYTHON_LIB_PATH_DEFAULT} CACHE PATH "path for installing Python module for Arbor.")
message(STATUS "Python module installation path: ${ARB_PYTHON_LIB_PATH}")

#
#install(DIRECTORY ${python_mod_path} DESTINATION ${ARB_PYTHON_LIB_PATH})
